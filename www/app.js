$(function () {

    function alertDismissed() {
        // noop
    }

    var taptime = new Hammer(document.getElementById("wrap"));

    taptime.on('tap', function(ev) {
        console.log(ev);
        NProgress.start();
        $.ajax({
            url: "https://api.github.com/zen",
            cache: false

        }).done(function (msg) {
            $("#zenMessage").text(msg);
            NProgress.done();

        }).fail(function () {
            navigator.notification.alert(
                "Error occured while fetching message.",
                alertDismissed,         // callback
                'Ooops',            // title
                'Okay'                  // buttonName
            );
            NProgress.done();
        });
    });

    taptime.on('doubletap', function(ev) {
      navigator.notification.alert(
          "Providing one zen thought at a time, via GitHub's API.",
          alertDismissed,         // callback
          'About',            // title
          'Okay'                  // buttonName
      );
    });

});
