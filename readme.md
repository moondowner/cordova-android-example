Source code for the Zen Thoughts app available on [Play Store](https://play.google.com/store/apps/details?id=com.thisismartin.android.zenthoughts).
====

Some links
-----

Cordova installation:

- manual: http://cordova.apache.org/docs/en/5.1.1/guide_cli_index.md.html#The%20Command-Line%20Interface

Cordova plugins:

- install plugman: `sudo npm install -g plugman`
- seach plugins:
http://plugins.cordova.io/npm/index.html?q=

Related:

- https://github.com/apache/cordova-plugin-whitelist#content-security-policy
- http://docs.phonegap.com/en/edge/cordova_notification_notification.md.html
- https://developer.ubuntu.com/en/apps/html-5/tutorials/cordova-camera-app-tutorial/
- http://cordova.apache.org/docs/en/5.1.1/config_ref_images.md.html#Icons%20and%20Splash%20Screens
- http://ionicframework.com/docs/guide/publishing.html
- https://cordova.apache.org/docs/en/edge/config_ref_index.md.html

Preparing build
-----

- create `plugins` and `platforms` directories
- add your platform, e.g. `cordova platform add android`
- check if the plugins `org.apache.cordova.dialogs` and `org.apache.cordova.dialogs` are installed with `cordova plugin ls`

Running build
-----

- Execute `cordova build browser`, and then open the `/platforms/browser/www/index.html` file.
- Or execute for some other platform, e.g. for Android: `cordova build android`, and `cordova emulate android` or `cordova run android` if your smartphone is available via `adb`.

Art
----

- App icon from icon set: https://www.iconfinder.com/iconsets/e-commerce-icon-set
- Android asset studio: https://romannurik.github.io/AndroidAssetStudio/index.html
- App icon color: `#2ECC71`
